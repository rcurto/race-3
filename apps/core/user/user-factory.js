(function (angular) {
    'use strict';
    angular.module('core').service('userFactory', userFactory);

    userFactory.$inject = ['$http', 'userService', '$filter'];

    function userFactory($http, userService, $filter) {
        return {
            isBasicPlan: isBasicPlan,
            isPremiumPlan: isPremiumPlan,
            getAllUsers: getAllUsers,
            getUserById: getUserById,
            store: store,
            login: login,
            getCount: getCount,
            auth: auth,
            logout: logout,
            getTopRecords: getTopRecords,
            getTopRunningTrailShoes: getTopRunningTrailShoes,
            getTopRunningRoadShoes: getTopRunningRoadShoes,
            getTopErgogenicAids: getTopErgogenicAids,
            getTopRunningWatchModel         : getTopRunningWatchModel,
            getQuickSideBarUsers            : getQuickSideBarUsers,
            getNewChatsMessages             : getNewChatsMessages,
            saveUserLastLoginDate           : saveUserLastLoginDate,
            getPendingNotifications         : getPendingNotifications,
            setPendingNotificationCount     : setPendingNotificationCount,
            getChkNotificationForNextTraning: getChkNotificationForNextTraning,
            pGetIntervalDate                : pGetIntervalDate
        };

        function isPremiumPlan(user) {

            if ( user!=undefined && user.trainingPlan == 'premium') {
                return true;
            }
            return false;
        }
        function isBasicPlan(user) {
            if ( user!=undefined && user.trainingPlan == 'basic') {
                return true;
            }
            return false;
        }

        function getTopRunningWatchModel(callback) {
            var promise = new Parse.Promise();

            var brands = {};
            var result = [];
            var query = new Parse.Query(Parse.User);
            query.notEqualTo('runningWatchModel', null);
            query.find({
                success: function (find) {
                    angular.forEach(find, function (row) {
                        if (row.get('runningWatchModel')) {
                            var ke1 = row.get('runningWatchModel').toLowerCase();
                            brands[ke1] = brands[ke1] == undefined ? {
                                brand: row.get('runningWatchModel'),
                                number: 0
                            } : brands[ke1];
                            brands[ke1].number++;
                        }
                    });
                    angular.forEach(brands, function (model) {
                        result.push(model)
                    });
                    if (callback){
                        callback(result);
                    }
                    promise.resolve(result);
                }
            });
            return promise;

        }

        function getTopErgogenicAids(callback) {
            var promise = new Parse.Promise();

            var brands = {};
            var result = [];
            var query = new Parse.Query(Parse.User);
            query.notEqualTo('ergogenicAids', null);
            query.find({
                success: function (find) {
                    angular.forEach(find, function (row) {
                        if (row.get('ergogenicAids')) {
                            var ke1 = row.get('ergogenicAids').toLowerCase();
                            brands[ke1] = brands[ke1] == undefined ? {
                                brand: row.get('ergogenicAids'),
                                number: 0
                            } : brands[ke1];
                            brands[ke1].number++;
                        }
                    });
                    angular.forEach(brands, function (model) {
                        result.push(model)
                    });
                    if (callback) {
                        callback(result);
                    }
                    promise.resolve(result);

                }
            });
            return promise;
        }

        function getTopRunningRoadShoes(callback) {
            var promise = new Parse.Promise();
            var shoes = {};
            var result = [];
            var query = new Parse.Query(Parse.User);
            query.notEqualTo('runningShoesRoadBrand', null);
            query.find({
                success: function (find) {
                    angular.forEach(find, function (row) {
                        if (row.get('runningShoesRoadBrand')) {
                            var ke1 = row.get('runningShoesRoadBrand').toLowerCase();
                            shoes[ke1] = shoes[ke1] == undefined ? {
                                brand: row.get('runningShoesRoadBrand'),
                                number: 0
                            } : shoes[ke1];
                            shoes[ke1].number++;
                        }
                    });
                    angular.forEach(shoes, function (model) {
                        result.push(model)
                    });
                    if (callback){
                        callback(result);
                    }
                    promise.resolve(result);
                }
            });
            return promise;
        }

        function getTopRunningTrailShoes(callback) {
            var promise = new Parse.Promise();
            var shoes = {};
            var result = [];
            var query = new Parse.Query(Parse.User);
            query.notEqualTo('runningShoesTrailBrand', null);
            // query.notEqualTo('runningShoesModel', null);
            // query.limit(count);
            query.find({
                success: function (find) {
                    angular.forEach(find, function (row) {
                        if (row.get('runningShoesTrailBrand')) {
                            var ke1 = row.get('runningShoesTrailBrand').toLowerCase();
                            shoes[ke1] = shoes[ke1] == undefined ? {
                                brand: row.get('runningShoesTrailBrand'),
                                number: 0
                            } : shoes[ke1];
                            shoes[ke1].number++;
                        }
                    });

                    angular.forEach(shoes, function (model) {
                        result.push(model)
                    });
                    if (callback){
                        callback(result);
                    }
                    promise.resolve(result);
                }
            });
            return promise;

        }

        function getTopRecords(groups, user) {
            var promises = [];
            var results = {};

            angular.forEach(groups, function (group) {
                if (group.show!=false){
                    var query = new Parse.Query(Parse.User);
                    (group.sorting == 'asc') ?   query.ascending(group.field) : query.descending(group.field);
                    query.notEqualTo(group.field, null);
                    var promise = query.find({
                        success: function (find) {
                            results[group.field] = find;
                        }
                    });
                    promises.push(promise);
                }
            });

            var finalPromise = new Parse.Promise();
            Parse.Promise.when(promises).then(function(){
                finalPromise.resolve( __formatTopRecords(results, user) );
            });
            return finalPromise;

        }


        function getCount(callback) {
            var query = new Parse.Query(Parse.User);
            query.count({
                success: function (count) {
                    callback(count);
                },
                error: function (error) {
                    alert("Error: " + error.code + " " + error.message);
                }
            });
        }

        function logout(callback) {
            Parse.User.logOut();
            if (callback != undefined) {
                callback();
            }
        }

        function auth(callback) {
            var currentUser = Parse.User.current();

            if (callback == undefined) {
                return currentUser ? getUserById(currentUser.id) : getUserById(null)
            } else {

                if (!currentUser) {
                    callback(null);
                    return;
                }
                getUserById(currentUser.id, function (user) {

                    setLoginUserInfo(user); //Add change by kazuyou@
//user.language = "catalan";
                    callback(user)
                })
            }
        }

        function login(username, password, callback) {
            Parse.User.logIn(username, password, {
                success: function (user) {
                    var user = userService(user);
                    if (user.get('isAdmin') == false) {
                        logout(function () {
                        });
                        callback({success: false, error: 'No Access'})
                    } else {
                        callback({success: true, user: userService(user)})
                    }

                },
                error: function (user, error) {
                    callback({success: false, error: error.message})
                }
            });
        }

        function store(data, callback) {
            var userObject = new Parse.User();
            var i;
            for (i in data) {
                userObject.set(i, data[i]);
            }

            userObject.save(null, {
                success: function (user) {
                    callback({success: true});
                },
                error: function (user, error) {
                    callback({success: false, error: error.message});
                }
            });
        }


        function getUserById(id, callback, wrapper) {
            var promise = new Parse.Promise();
            var query = new Parse.Query(Parse.User);
            query.include('role');
            query.get(id).then(function (user) {
                var answer = wrapper == undefined ? userService(user) : user;
                if (callback) {
                    callback(answer)
                }
                promise.resolve(answer);
            }, function (error) {
                promise.resolve(null);
                if (callback) {
                    callback(null)
                }
            });
            return promise;
        }


        function getAllUsers(callback) {
            var promise = new Parse.Promise();

            var query = new Parse.Query(Parse.User);
            query.count({
                success: function (count) {
                    var result = [];
                    var pages = Math.round(count / 1000) + 1;
                    var back = 0;
                    for (var i = 0; i < pages; i++) {
                        query.include()
                        query.skip(i * 1000).limit(1000).find({
                            success: function (data) {
                                var users = [];
                                for (var j in data) {
                                    users.push(userService(data[j]))
                                }
                                back++;
                                result = result.concat(users);
                                if (back == pages) {
                                    if (callback){
                                        callback(result)
                                    }else{
                                        promise.resolve(result);

                                    }
                                }
                            }
                        });
                    }

                }
            });
            return promise;

        }

        /****
         # Maker         : Created by kazuyou@
         # Date          : 2016/03/14
         # Description   : Set login user info to localStorage
         ****/
        function setLoginUserInfo(userInfo) {

            localStorage.setItem("username", userInfo.username);
            localStorage.setItem("isCoach", userInfo.isCoach());
            localStorage.setItem("isAthlete", userInfo.isAthlete());

            if (userInfo.photo) {
                localStorage.setItem("userphotourl", userInfo.photo.url());
            }
            else {
                localStorage.setItem("userphotourl", "apps/chatApp/themes/default/images/nophoto.png");
            }
        }

        /****
         # Maker         : Created by kazuyou@
         # Date          : 2016/2/28
         # Description   : Get userlist beside login user
         ****/
        function getQuickSideBarUsers(callback) {

            var currentUserId = new Parse.User.current().id;

            var no_image_photo = "apps/chatApp/themes/default/images/nophoto.png";

            var query = new Parse.Query(Parse.User);

            query.include('role');
            query.descending('lastLoginDate');

            var sidebarUserArray = new Array();
            var sidebarStaffArray = new Array();
            var sidebarAthleteArray = new Array();

            query.find().then(function (results) {
                /* Go Through Each Post */
                for (var i in results) {

                    /* Set obj to current post*/
                    var obj = results[i];

                    /* Get User's UserId */
                    var userId = obj.id;

                    /* Get User's City */
                    var pCity = obj.get("city");
                    
                    if (!pCity) pCity = "";

                    /* Get User's UserName */
                    var username = obj.get("username");

                    /* Get User's FirstName */
                    var firstName = obj.get("firstName");

                    if (!firstName) firstName = "";

                    /* Get User's LastName */
                    var lastName = obj.get("lastName");
                    if (!lastName) lastName = "";

                    var regularUserName = "";

                    if (firstName == "" && lastName == "")
                        regularUserName = "-No Name-(" + username + ")";
                    else
                        regularUserName = firstName + " " + lastName;

                    /* Get User Photo */
                    var userPhoto = obj.get("photo");

                    if (!userPhoto) userPhoto = no_image_photo;
                    else           userPhoto = userPhoto._url;

                    /* Get Post Author's Name */
                    var roleName = obj.get("role").get("name");

                    /* Get User's Last Login Date */
                    var lastLoginDate = obj.get("lastLoginDate");

                    if (!lastLoginDate)
                        lastLoginDate = "";
                    else {
                        var options = {
                            year: "numeric",
                            month: "short",
                            day: "numeric",
                            hour: "2-digit",
                            minute: "2-digit",
                            second: "2-digit"
                        };

                        var lastLoginDate = new Date(lastLoginDate).toLocaleTimeString("en-us", options);

                        lastLoginDate = "Last seen " + lastLoginDate;
                    }

                    /* Let's Put the SideBar User Information in an Array as an Object*/
                    var tmpAry = {

                        regularUserName: regularUserName,
                        username: username,
                        rolename: roleName,
                        userphoto: userPhoto,
                        userid: userId,
                        clsChatStatus: 'badge badge-danger',
                        actionChatStatus: 'offline',
                        onlineStatusUserCount: 0,
                        lastLoginDate       : lastLoginDate,
                        city                : pCity
                    };

                    if (userId != currentUserId && roleName && (roleName == 'coach' || roleName == 'nutritionist' || roleName == 'psychologist')) {
                        sidebarStaffArray.push(tmpAry);
                    }
                    if (userId != currentUserId && roleName && roleName == 'athlete') {
                        sidebarAthleteArray.push(tmpAry);
                    }
                }
                sidebarUserArray = {sidebarStaffArray, sidebarAthleteArray};

                callback(sidebarUserArray);
            });
        }

        /****
         # Maker         : Created by kazuyou@
         # Date          : 2016/2/28
         # Description   : Get chatHistory of login user
         ****/
        function getNewChatsMessages(userlist, callback) {

            var currentUserId = new Parse.User.current().id;

            var objSidebarChatMsg = Parse.Object.extend("ChatsMessages");
            var query = new Parse.Query(objSidebarChatMsg);

            query.equalTo("toUser", "" + currentUserId);  //
            query.equalTo("chatStatus", "new");             //

            query.find().then(function (results) {
                var staffUserList = userlist.sidebarStaffArray;
                var athleteUserList = userlist.sidebarAthleteArray;

                for (var i = 0, len = staffUserList.length; i < len; i++) {

                    var tmp = staffUserList[i];
                    var userCount = 0;

                    for (var j = 0, len1 = results.length; j < len1; j++) {
                        var tmp1 = results[j];

                        if (tmp.userid == tmp1.get("fromUser")) {
                            userCount++;
                        }
                    }
                    staffUserList[i].onlineStatusUserCount = userCount;
                }

                for (var i = 0, len = athleteUserList.length; i < len; i++) {

                    var tmp = athleteUserList[i];
                    var userCount = 0;

                    for (var j = 0, len1 = results.length; j < len1; j++) {
                        var tmp1 = results[j];
                        if (tmp.userid == tmp1.get("fromUser")) {
                            userCount++;
                        }
                    }
                    athleteUserList[i].onlineStatusUserCount = userCount;
                }

                userlist.sidebarStaffArray = staffUserList;
                userlist.sidebarAthleteArray = athleteUserList;

                callback(userlist);
            });
        }

        /****
         # Maker         : Created by kazuyou@
         # Date          : 2016/3/09
         # Description   : Get last session date info of userlist
         ****/
        function saveUserLastLoginDate() {

            var userId = new Parse.User.current().id;

            var objUser = Parse.Object.extend("User");
            var query = new Parse.Query(objUser);

            query.equalTo("objectId", userId);

            query.find({
                success: function (results) {
                    for (var i = 0, len = results.length; i < len; i++) {
                        var result = results[i];

                        var logindate = new Date();

                        result.set("lastLoginDate", logindate);

                        result.save(null, {
                            success: function (obj) {
                                console.log("LastLogin Date Successfully saved");
                            },
                            error: function (err) {
                                console.log("LastLogin Date Not successfully saved");
                            }

                        });
                    }
                },
                error: function (error) {
                    alert("Error: " + error.code + " " + error.message);
                }
            });
        }


        var flagWeeklyFeedback          = false;
        var flagRaceFeedback            = false;
        var flagCoachTip                = false;
        var flagRequest                 = false;
        var flagJoinRace                = false;

        var objPendingNotificationList  = {};

        function getPendingNotifications(user) {
            var promise = new Parse.Promise();


           // getWeeklyFeedbacks(callback);
            //getRaceFeedbacks(callback);
           // getCoachTip(callback);
           // getRequestCoachComment(callback);
            //getJoinRaceNotification(callback);

            eventFactory.getUnreadEvents(user.parseObject).then(function(response){
                promise.resolve(response);
            });
            return promise;

        }

        function setPendingNotificationCount(user) {

            var totalPendingCount = 0;
            var currentUserId = new Parse.User.current().id;

            var weeklyFeedbackObject = Parse.Object.extend("Feedback");
            var weeklyFB_Query = new Parse.Query(weeklyFeedbackObject);

            var userObject = Parse.Object.extend("User");
            var matchQuery = new Parse.Query(userObject);

            matchQuery.equalTo("objectId", currentUserId);
            weeklyFB_Query.matchesQuery("user", matchQuery);


            weeklyFB_Query.equalTo("coachFeedback", true);
            weeklyFB_Query.equalTo("flgUserView", false);

            weeklyFB_Query.count({
                success: function (number) {
                    // There are number instances of MyClass.

                    if (!isNaN(number)) {
                        totalPendingCount += number;
                    }

                    var atletaCursaObject = Parse.Object.extend("atleta_cursa");
                    var atletaCursa_Query = new Parse.Query(atletaCursaObject);

                    atletaCursa_Query.equalTo("isCoachFull", true);

                    var atletaCursa_OR_Query = new Parse.Query(atletaCursaObject);
                    atletaCursa_OR_Query.notEqualTo("coachTip", '');

                    var mainQuery = Parse.Query.or(atletaCursa_Query, atletaCursa_OR_Query);

                    mainQuery.equalTo("flgUserView", false);
                    mainQuery.matchesQuery("user", matchQuery);

                    mainQuery.count({
                        success: function (number1) {
                            // There are number instances of MyClass.
                            if (!isNaN(number1)) {
                                totalPendingCount += number1;
                            }

                            var startTrain = user.get("startTrain");

                            if (startTrain && (getChkNotificationForNextTraning(startTrain) == true)) {
                                totalPendingCount++;
                            }

                            var requestObject = Parse.Object.extend("Request");
                            var requestQuery = new Parse.Query(requestObject);

                            requestQuery.notEqualTo("closed",       true);
                            requestQuery.notEqualTo("coachComment", "");

                            requestQuery.matchesQuery("user", matchQuery);

                            requestQuery.count({
                                success: function (number2) {
//console.log("###########################");
//console.log(number2);
//console.log("###########################");                                  
                                    if (!isNaN(number2)) {
                                        totalPendingCount += number2;
                                    }
                                    if ($('.top-menu .dropdown-notification')) {
                                        if (totalPendingCount > 0) {
                                            $('.top-menu .dropdown-notification').find(".dropdown-toggle > span").addClass("badge-default").removeClass("badge-warning");
                                        } else {
                                            $('.top-menu .dropdown-notification').find(".dropdown-toggle > span").addClass("badge-warning").removeClass("badge-default");
                                        }
                                        if (localStorage.getItem("isAthlete") == 'true') {
                                            $('.top-menu .dropdown-notification').find(".dropdown-toggle > span").text(totalPendingCount);
                                        }
                                    }

                                },
                                error: function(error){
                                    // error is an instance of Parse.Error.
                                }
                            });
                        },
                        error: function (error) {
                            // error is an instance of Parse.Error.
                        }
                    });
                },
                error: function (error) {
                    // error is an instance of Parse.Error.
                }
            });
        }


        function getWeeklyFeedbacks(callback) {
//console.log("hre");
            var currentUserId = new Parse.User.current().id;

            var extObject = Parse.Object.extend("Feedback");
            var mainQuery = new Parse.Query(extObject);

            var userObject = Parse.Object.extend("User");
            var matchQuery = new Parse.Query(userObject);

            matchQuery.equalTo("objectId", currentUserId);
            mainQuery.matchesQuery("user", matchQuery);

            mainQuery.equalTo("coachFeedback", true);
            mainQuery.equalTo("flgUserView", false);

            var aryWeeklyFeedback = new Array();
            mainQuery.find().then(function (results) {
                /* Go Through Each Post */
                for (var i in results) {

                    /* Set obj to current post*/
                    var obj = results[i];

                    // Get User's Last Login Date
                    var lastupdatedDate = new Date(obj.get("updatedAt")).getTime();
                    var pInterval = pGetIntervalDate(lastupdatedDate);


                    var tmpAry = {
                        objectId        : obj.id,
                        intervalMinute  : pInterval,
                        coachComment    : obj.get("coachComment"),
                        timestampe      : lastupdatedDate,
                        noticeMsg       : $filter('translate')("Coach comment your weekly feedback")
                    };

                    aryWeeklyFeedback.push(tmpAry);
                }
                objPendingNotificationList['weeklyFeedback'] = aryWeeklyFeedback;
                flagWeeklyFeedback = true;

                checkPendingNotificationProceedStatus(callback);
            });
        }

        /****
         # Maker         : Created by kazuyou@
         # Date          : 2016/03/13
         # Description   : Get Race Feedback pending notification list
         ****/
        function getRaceFeedbacks(callback) {

            var currentUserId = new Parse.User.current().id;

            var cursaObject = Parse.Object.extend("atleta_cursa");
            var mainQuery = new Parse.Query(cursaObject);

            var userObject = Parse.Object.extend("User");
            var matchQuery = new Parse.Query(userObject);

            matchQuery.equalTo("objectId", currentUserId);
            mainQuery.matchesQuery("user", matchQuery);

            mainQuery.equalTo("isCoachFull", true);
            mainQuery.equalTo("flgUserView", false);

            var aryRaceFeedback = new Array();

            mainQuery.find().then(function (results) {
                /* Go Through Each Post */
                for (var i in results) {

                    /* Set obj to current post*/
                    var obj = results[i];

                    // Get User's Last Login Date
                    var lastupdatedDate = new Date(obj.get("updatedAt")).getTime();
                    var currentDate = new Date().getTime();

                    var pInterval = pGetIntervalDate(lastupdatedDate);


                    var tmpAry = {
                        objectId        : obj.id,
                        intervalMinute  : pInterval,
                        coachComment    : obj.get("coachComments"),
                        timestampe      : lastupdatedDate,
                        noticeMsg       : $filter('translate')("Coach comment your race feedback")
                    };

                    aryRaceFeedback.push(tmpAry);
                }
                objPendingNotificationList['raceFeedback'] = aryRaceFeedback;
                flagRaceFeedback = true;

                checkPendingNotificationProceedStatus(callback);
            });
        }

        /****
         # Maker         : Created by kazuyou@
         # Date          : 2016/03/16
         # Description   : Get coachtip list for new race
         ****/
        function getCoachTip(callback) {

            var currentUserId = new Parse.User.current().id;
//console.log("UserId=" + currentUserId);
            var cursaObject = Parse.Object.extend("atleta_cursa");
            var mainQuery = new Parse.Query(cursaObject);

            mainQuery.include('nomcursa');

            var userObject = Parse.Object.extend("User");
            var matchQuery = new Parse.Query(userObject);

            matchQuery.equalTo("objectId", currentUserId);
            mainQuery.matchesQuery("user", matchQuery);

            mainQuery.notEqualTo("coachTip", '');
            mainQuery.equalTo("flgUserView", false);

            var aryCoachTip = new Array();

            mainQuery.find().then(function (results) {
                /* Go Through Each Post */

                for (var i in results) {

                    /* Set obj to current post*/
                    var obj = results[i];
//console.log(obj.get("nomcursa").get("nom"));                    
//console.log(obj.get("coachTip"));                    
                    // Get User's Last Login Date
                    var lastupdatedDate = new Date(obj.get("updatedAt")).getTime();
                    var pInterval = pGetIntervalDate(lastupdatedDate);

                    // Let's Put the SideBar Weekly Feedback Information in an Array as an Object
                    var message = $filter('translate')("The race tips for the %race% are ready");
                    message = message.replace('%race%',  obj.get("nomcursa").get("nom"));
                    var tmpAry = {
                        objectId        : obj.id,
                        intervalMinute  : pInterval,
                        coachComment    : obj.get("coachTip"),
                        timestampe      : lastupdatedDate,
                        noticeMsg       : message
                    };

                    aryCoachTip.push(tmpAry);
                }
                objPendingNotificationList['coachTip'] = aryCoachTip;
                flagCoachTip = true;

                checkPendingNotificationProceedStatus(callback);
            });
        }
         /****
         # Maker         : Created by kazuyou@
         # Date          : 2016/03/18
         # Description   : Get Request Coach Comment pending notification list
         ****/
        function getRequestCoachComment(callback) {

            var currentUserId = new Parse.User.current().id;

            var extObject = Parse.Object.extend("Request");
            var mainQuery = new Parse.Query(extObject);

            var userObject = Parse.Object.extend("User");
            var matchQuery = new Parse.Query(userObject);

            matchQuery.equalTo("objectId", currentUserId);
            mainQuery.matchesQuery("user", matchQuery);

            mainQuery.notEqualTo("closed",          true);
            mainQuery.notEqualTo("coachComment",    "");

            var aryRequest = new Array();
            mainQuery.find().then(function (results) {
                /* Go Through Each Post */
                for (var i in results) {

                    /* Set obj to current post*/
                    var obj = results[i];

                    // Get User's Last Login Date
                    var lastupdatedDate = new Date(obj.get("updatedAt")).getTime();
                    var pInterval = pGetIntervalDate(lastupdatedDate);
                    // Let's Put the SideBar Weekly Feedback Information in an Array as an Object

                    var message = $filter('translate')("Coach comment your %subject% request");
                    message = message.replace('%subject%',  obj.get("subject"));
                    var tmpAry = {
                        objectId        : obj.id,
                        intervalMinute  : pInterval,
                        coachComment    : obj.get("coachComment"),
                        timestampe      : lastupdatedDate,
                        noticeMsg       : message
                    };

                    aryRequest.push(tmpAry);
                }
                objPendingNotificationList['request'] = aryRequest;
                flagRequest = true;

                checkPendingNotificationProceedStatus(callback);
            });
        }
        /****
         # Maker         : Created by kazuyou@
         # Date          : 2016/03/19
         # Description   : Get notification when athlete join new race
         ****/
        //function getJoinRaceNotification(callback) {
        //
        //
        //    var currentUserId = new Parse.User.current().id;
        //
        //    var extObject   = Parse.Object.extend("Event");
        //    var mainQuery   = new Parse.Query(extObject);
        //
        //    mainQuery.include("curses");
        //
        //    var userObject = Parse.Object.extend("User");
        //    var matchQuery = new Parse.Query(userObject);
        //
        //    matchQuery.equalTo("objectId", currentUserId);
        //    mainQuery.matchesQuery("user", matchQuery);
        //
        //    mainQuery.notEqualTo("flgUserView", true);
        //
        //    var aryJoinRace = new Array();
        //    mainQuery.find().then(function (results) {
        //        /* Go Through Each Post */
        //        for (var i in results) {
        //
        //            /* Set obj to current post*/
        //            var obj = results[i];
        //
        //            // Get User's Last Login Date
        //            var lastupdatedDate = new Date(obj.get("updatedAt")).getTime();
        //            var pInterval       = pGetIntervalDate(lastupdatedDate);
        //            // Let's Put the SideBar Weekly Feedback Information in an Array as an Object
        //            var message = $filter('translate')("The race %race_name% has been added to your Races");
        //            message = message.replace('%race_name%', obj.get("curses").get("nom"));
        //
        //            var tmpAry = {
        //                objectId        : obj.id,
        //                intervalMinute  : pInterval,
        //                coachComment    : "",
        //                timestampe      : lastupdatedDate,
        //                noticeMsg       : message,
        //
        //            };
        //
        //            aryJoinRace.push(tmpAry);
        //        }
        //        objPendingNotificationList['joinrace'] = aryJoinRace;
        //        flagJoinRace = true;
        //
        //       checkPendingNotificationProceedStatus(callback);
        //    });
        //}
        /****
         # Maker         : Created by kazuyou@
         # Date          : 2016/03/13
         # Description   : Check pending notification getting synchronize status
         ****/
        function checkPendingNotificationProceedStatus(callback) {
            if (flagWeeklyFeedback == true && flagRaceFeedback == true && flagCoachTip == true && flagRequest == true && flagJoinRace == true) {

                var aryPendingNotification = new Array();

                if(objPendingNotificationList['weeklyFeedback'].length > 0){
                        for(var i= 0, len= objPendingNotificationList['weeklyFeedback'].length; i< len; i++){
                            
                            var tmp = objPendingNotificationList['weeklyFeedback'][i];

                            aryPendingNotification.push({
                                objectId        : tmp.objectId,
                                noticeMsg       : tmp.noticeMsg,
                                intervalMinute  : tmp.intervalMinute,
                                coachComment    : tmp.coachComment,
                                timestampe      : tmp.timestampe,
                                tblName         : "Feedback",
                                icon            : "icon-speech",
                                noticeTitle     : $filter('translate')("Weekly Notification"),
                                multishow       : "1",
                                alink           : "feedbacks",
                                showMsg         : true,
                                removable       : true
                            });
                        }
                    }
                    if(objPendingNotificationList['raceFeedback'].length > 0){
                        for(var i= 0, len= objPendingNotificationList['raceFeedback'].length; i< len; i++){
                            
                            var tmp = objPendingNotificationList['raceFeedback'][i];

                            aryPendingNotification.push({
                                objectId        : tmp.objectId,
                                noticeMsg       : tmp.noticeMsg,
                                intervalMinute  : tmp.intervalMinute,
                                coachComment    : tmp.coachComment,
                                timestampe      : tmp.timestampe,
                                tblName         : "atleta_cursa",
                                icon            : "icon-trophy",
                                noticeTitle     : $filter('translate')("Race Notification"),
                                multishow       : "1",
                                alink           : "feedbacks",
                                showMsg         : true,
                                removable       : true
                            });
                        }
                    }
                    if(objPendingNotificationList['coachTip'].length > 0){
                        for(var i= 0, len= objPendingNotificationList['coachTip'].length; i< len; i++){
                            
                            var tmp = objPendingNotificationList['coachTip'][i];

                            aryPendingNotification.push({
                                objectId        : tmp.objectId,
                                noticeMsg       : tmp.noticeMsg,
                                intervalMinute  : tmp.intervalMinute,
                                coachComment    : tmp.coachComment,
                                timestampe      : tmp.timestampe,
                                tblName         : "atleta_cursa",
                                icon            : "fa fa-area-chart",
                                noticeTitle     : $filter('translate')("Coach Tip"),
                                multishow       : "1",
                                alink           : "requests",
                                showMsg         : true,
                                removable       : true
                            });
                        }
                    }
                    if(objPendingNotificationList['request'].length > 0){
                        for(var i= 0, len= objPendingNotificationList['request'].length; i< len; i++){
                            
                            var tmp = objPendingNotificationList['request'][i];

                            aryPendingNotification.push({
                                objectId        : tmp.objectId,
                                noticeMsg       : tmp.noticeMsg,
                                intervalMinute  : tmp.intervalMinute,
                                coachComment    : tmp.coachComment,
                                timestampe      : tmp.timestampe,
                                tblName         : "Request",
                                icon            : "icon-call-out",
                                noticeTitle     : "",
                                multishow       : "2",
                                alink           : "requests",
                                showMsg         : false,
                                removable       : true
                            });
                        }
                    }
                    if(objPendingNotificationList['joinrace'].length > 0){
                        for(var i= 0, len= objPendingNotificationList['joinrace'].length; i< len; i++){
                            
                            var tmp = objPendingNotificationList['joinrace'][i];

                            aryPendingNotification.push({
                                objectId        : tmp.objectId,
                                noticeMsg       : tmp.noticeMsg,
                                intervalMinute  : tmp.intervalMinute,
                                coachComment    : tmp.coachComment,
                                timestampe      : tmp.timestampe,
                                tblName         : "Event",
                                icon            : "icon-badge",
                                noticeTitle     : "",
                                multishow       : "1",
                                alink: "race-my",

                                showMsg         : false,
                                removable       : true
                            });
                        }
                    }

                callback(aryPendingNotification);
            }
        }
        function setPendingNotificaion(user){
            getPendingNotifications(function(pendingNotificationList){

                if(user.get("startTrain") && getChkNotificationForNextTraning(user.get("startTrain")) == true){
                    
                    var pStartTrain                 = new Date(user.get("startTrain"));

                    pStartTrain.setDate(pStartTrain.getDate() + 13);

                    var pTimeStamp          = pStartTrain.getTime();
                    var pNextTraningInterval = pGetIntervalDate(pTimeStamp);

                    pendingNotificationList.push({
                        objectId        : "",
                        noticeMsg       : $filter('translate')("Remember to write your comments for the next training plan"),
                        intervalMinute  : pNextTraningInterval,
                        coachComment    : "",
                        timestampe      : pTimeStamp,
                        tblName         : "",
                        icon            : "icon-notebook",
                        noticeTitle     : ""
                    });
                }
               
                if(angular.element(document.getElementById('header_notification_bar')).scope().env){
                    angular.element(document.getElementById('header_notification_bar')).scope().env.pendingNotificationList = pendingNotificationList;
                }
                //angular.element(document.getElementById('header_notification_bar')).scope().env.pendingNotificationList = pendingNotificationList;
                    console.log("$apply");
                angular.element(document.getElementById('header_notification_bar')).scope().$apply();

                HeaderUIToastr.init();

            });
        }
        /****
         # Maker         : Created by kazuyou@
         # Date          : 2016/03/17
         # Description   : Check current date is 2 week date from indicate date or not.
         ****/
        function getChkNotificationForNextTraning(pDate) {

            var targetDate      = new Date(pDate);
            var targetEndDate   = new Date(pDate);

            var nowDate = new Date();

            targetDate.setDate(targetDate.getDate() + 13);
            targetEndDate.setDate(targetEndDate.getDate() + 28);

            targetDate      = pGetDateFormat(targetDate.getFullYear(),      (targetDate.getMonth() + 1),    targetDate.getDate());
            targetEndDate   = pGetDateFormat(targetEndDate.getFullYear(),   (targetEndDate.getMonth() + 1), targetEndDate.getDate());
            nowDate         = pGetDateFormat(nowDate.getFullYear(),         (nowDate.getMonth() + 1),       nowDate.getDate());

//console.log("targetDate=" + targetDate);
//console.log("targetEndDate=" + targetEndDate);
//console.log("nowDate=" + nowDate);

            if(nowDate > targetDate && nowDate < targetEndDate){
                return true;
            }else{
                return false;
            }
        }
         /****
         # Maker         : Created by kazuyou@
         # Date          : 2016/03/17
         # Description   : Get Date with String type Format.
         ****/
        function pGetDateFormat(pYear, pMonth, pDate) {

            if((""+pMonth).length < 2) 
                pMonth = "0" + pMonth;
            if((""+pDate).length < 2) 
                pDate = "0" + pDate;

            return ("" + pYear + pMonth + pDate);
        }
        /****
         # Maker         : Created by kazuyou@
         # Date          : 2016/03/17
         # Description   : Get interval Date From now.
         ****/
        function pGetIntervalDate(pTimeStamp) {

            var currentDate     = new Date().getTime();

            var pMinute         = Math.floor((Math.floor(currentDate - pTimeStamp) / 1000) / 60);
            var pHours          = Math.floor(Math.floor((Math.floor(currentDate - pTimeStamp) / 1000) / 3660));
            var pDays           = Math.floor((Math.floor(currentDate - pTimeStamp) / 1000) / (3600 * 24));
//console.log(pDays   + "days");
//console.log(pHours   + "hrs");
//console.log(pMinute   + "mins");

            if(pDays   > 0)     return (pDays   + $filter('translate')("days") );
            if(pHours  > 0)     return (pHours  + $filter('translate')("hrs") );
            if(pMinute > 0)     return (pMinute + $filter('translate')("mins") );

            return $filter('translate')("Just Now");
        }

        function __formatTopRecords(records, user) {
            var result = {};
            angular.forEach(records, function (rows, index) {
                var total = rows.length;
                var now = 0;
                var time = 0;
                var current = 0;
                angular.forEach(rows, function (row) {
                    now++;
                    if (row.id == user.getId()) {
                        time = row.get(index);
                        current = now;
                        return true;
                    }
                });

                if (current == 1) {
                    result[index] = {place: $filter('translate')('NUMBER ONE'), time: time};
                } else {
                    var percent = current * (100 / total);
                    if (percent <= 5) {
                        result[index] = {place: $filter('translate')('PODIUM'), time: time};
                    } else if (percent > 5 && percent <= 10) {
                        result[index] = {place: $filter('translate')('TOP 10%'), time: time};
                    } else if (percent > 10 && percent <= 30) {
                        result[index] = {place: $filter('translate')('TOP 30%'), time: time};
                    } else if (percent > 30 && percent <= 50) {
                        result[index] = {place: $filter('translate')('TOP 50%'), time: time};
                    } else {
                        result[index] = {place: $filter('translate')('TOP 100%'), time: time};
                    }
                }
            });
            return result;
        }

    }
})(angular);

