(function (angular) {
    'use strict';
    angular.module('core').service('cursesService', cursesService);

    cursesService.$inject = ['atletaCursaFactory','$filter'];
    function cursesService(atletaCursaFactory,$filter) {


        return function (parseObject) {

            var object = angular.copy(parseObject.attributes);

            object.parseObject = parseObject;
            object.attributes = angular.copy(parseObject.attributes);
            object.attributes.createdAt = moment(object.parseObject.createdAt).format('DD-MM-YYYY');
            object.shortData = moment( object.data ).utc().format('DD MMM');
            object.printData = moment(parseObject.get('data')).format('DD-MM-YYYY');
            object.isPastRace = moment().isBefore( parseObject.get('data') );

            if ( parseObject.get('circuit') ){
                object.circuitId = parseObject.get('circuit').id;
                object.attributes.circuit = parseObject.get('circuit')
            }
            if ( parseObject.get('country') ){
                object.countryId = parseObject.get('country').id;
                object.attributes.country =parseObject.get('country')
            }
            object.zoneId = parseObject.get('zone')!=undefined ? parseObject.get('zone').id : null;

            object.get = function (key) {
                return this.attributes[key];
            };
            object.getId = function () {
                return this.parseObject.id;
            };
            object.getCreatedAt = function () {
                return this.parseObject.createdAt;
            };
            object.getCurseData = function () {
                return moment(this.get('data')).format('DD-MM-YYYY')
            };
            object.getCurseJSData = function () {
                return moment(this.get('data')).toDate()
            };
            object.getFreeFormatData = function (format) {
                return moment(this.get('data')).format(format)
            };

            object.update = function (data, callback) {
                var i;
                for (i in data) {
                    if (i=='parseObject'){
                        continue;
                    }
                    this.parseObject.set(i, data[i]);
                }
                this.parseObject.save(data, {
                    success: function (answer) {
                        callback({success: true});
                    },
                    error: function (answer, error) {
                        callback({success: false, error: error.message});
                    }
                })
            };

            object.delete = function (callback) {
                this.parseObject.destroy({
                    success: function (myObject) {
                        callback({success:true})
                    },
                    error: function (myObject, error) {
                        callback({success:false,error:error})
                    }
                });
            };

            object.join = function (objective, user, callback) {
                user.joinCourse( object.parseObject );
                var exist = false;
                atletaCursaFactory.getByAthlete(user, function(courses){
                    angular.forEach(courses, function(course){
                        if ( course.get('nomcursa') && course.get('nomcursa').objectId==object.getId() ){
                            exist = true;
                        }
                    });
                    if ( exist==false ){
                        atletaCursaFactory.store({
                            'nomcursa' : object.parseObject,
                            'user' : user.parseObject,
                            'objective': objective
                        },callback);
                        object.parseObject.add("joined", {"objectId":user.getId(),"__type":"Pointer","className":"_User"});
                        object.parseObject.save()
                    }
                })
            };

            object.getJoined = function(){
                var joined = object.parseObject.get('joined');
                var users = [];


                angular.forEach(joined, function(user){

                    if ( user && user.id!=undefined ){
                        var firstName = user.get('firstName')==undefined ? '': user.get('firstName');
                        var lastName = user.get('lastName')==undefined ? '': user.get('lastName');
                        users.push( firstName+' '+lastName )
                    }

                });
                if ( users.length>0 ){
                    return users.join(', ');
                }else{
                    return $filter('translate')('No athletes');
                }

            };

            return object;

        }
    }
})(angular);

