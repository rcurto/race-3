/**
/*
ClassName	:	quicksidebarController
Maker		:	Add by kazuyou@
Date		:	
Description : Core script to handle the entire theme and core functions
*/
var QuickSidebar = function () {
	
	var _recipientname;
	var _private_channel;
	var _toggleSidebarOpen = true;

    // Handles quick sidebar toggler
    var handleQuickSidebarToggler = function () {
        // quick sidebar toggler
        $('.top-menu .dropdown-quick-sidebar-toggler a, .page-quick-sidebar-toggler').click(function (e) {

           $('body').toggleClass('page-quick-sidebar-open');
        });
    };
    // Handles quick sidebar chats
    var handleQuickSidebarChat = function () {
        var wrapper = $('.page-quick-sidebar-wrapper');
        var wrapperChat = wrapper.find('.page-quick-sidebar-chat');

        var initChatSlimScroll = function () {
            var chatUsers = wrapper.find('.page-quick-sidebar-chat-users');
            var chatUsersHeight;

            chatUsersHeight = wrapper.height() - wrapper.find('.nav-justified > .nav-tabs').outerHeight();

            // chat user list 
            Metronic.destroySlimScroll(chatUsers);
            chatUsers.attr("data-height", chatUsersHeight);
            Metronic.initSlimScroll(chatUsers);

            var chatMessages = wrapperChat.find('.page-quick-sidebar-chat-user-messages');
            var chatMessagesHeight = chatUsersHeight - wrapperChat.find('.page-quick-sidebar-chat-user-form').outerHeight() - wrapperChat.find('.page-quick-sidebar-nav').outerHeight();

            // user chat messages 
            Metronic.destroySlimScroll(chatMessages);
            chatMessages.attr("data-height", chatMessagesHeight);
            Metronic.initSlimScroll(chatMessages);
        };

        initChatSlimScroll();
        Metronic.addResizeHandler(initChatSlimScroll); // reinitialize on window resize

        wrapper.find('.page-quick-sidebar-chat-users .media-list > .media').click(function () {
//			if($(this).find('.media-status').attr('actionchatstatus') == "online" || 
//			   $(this).find('.media-status').attr('actionchatstatus') == "onleave"){

				var loginUser  = localStorage.getItem("username");

				_recipientname = $(this).find('.media-body > h4').html();
				
				var private_channel = "";

				if(loginUser > _recipientname) 
					_private_channel = loginUser +_recipientname;
				else 
					_private_channel = _recipientname + loginUser;

				angular.element(document.getElementById('quicksidebarController')).scope().env._chatToUser = _recipientname;
				angular.element(document.getElementById('quicksidebarController')).scope().setToUserid(_recipientname);
				angular.element(document.getElementById('quicksidebarController')).scope().updateSideBarChatStatus();
				angular.element(document.getElementById('quicksidebarController')).scope().setViewChattingUserStatus($(this).find('.media-status').attr('actionchatstatus'));
				angular.element(document.getElementById('quicksidebarController')).scope()._usrPhotSrc = localStorage.getItem("userphotourl");
				angular.element(document.getElementById('quicksidebarController')).scope().updateSubscription(_private_channel);
				angular.element(document.getElementById('quicksidebarController')).scope().setHistory();

				wrapperChat.addClass("page-quick-sidebar-content-item-shown");
//			}

        });
		/********************************************************/

        wrapper.find('.page-quick-sidebar-chat-user .page-quick-sidebar-back-to-list').click(function () {
			angular.element(document.getElementById('quicksidebarController')).scope().updateZeroMsgCount();
			wrapperChat.removeClass("page-quick-sidebar-content-item-shown");
        });

		var handleChatMessagePost = function (e) {
			
			var chatContainer = wrapperChat.find(".page-quick-sidebar-chat-user-messages");

			var getLastPostPos = function() {
				var height = 0;
				chatContainer.find(".post").each(function() {
					height = height + $(this).outerHeight();
				});

				return height;
			}; 

			var input = wrapperChat.find('.page-quick-sidebar-chat-user-form .form-control');

			var msg = input.val();

			if(!msg || msg ==  "") return false;
			
			angular.element(document.getElementById('quicksidebarController')).scope().saveSideBarChatMessage(msg);
			angular.element(document.getElementById('quicksidebarController')).scope().getOneUserNewMsgCount(msg);

			chatContainer.slimScroll({
				scrollTo: getLastPostPos()
			});

			input.val("");

			return false;
		}
        
		wrapperChat.find('.page-quick-sidebar-chat-user-form .btn').click(handleChatMessagePost);
        wrapperChat.find('.page-quick-sidebar-chat-user-form .form-control').keypress(function (e) {
			
			 if (e.which == 13) {
				handleChatMessagePost(e);
			 }	
        });

    };

    // Handles quick sidebar tasks
    var handleQuickSidebarAlerts = function () {
        var wrapper = $('.page-quick-sidebar-wrapper');
        var wrapperAlerts = wrapper.find('.page-quick-sidebar-alerts');

        var initAlertsSlimScroll = function () {
            var alertList = wrapper.find('.page-quick-sidebar-alerts-list');
            var alertListHeight;

            alertListHeight = wrapper.height() - wrapper.find('.nav-justified > .nav-tabs').outerHeight();

            // alerts list 
            Metronic.destroySlimScroll(alertList);
            alertList.attr("data-height", alertListHeight);
            Metronic.initSlimScroll(alertList);
        };

        initAlertsSlimScroll();
        Metronic.addResizeHandler(initAlertsSlimScroll); // reinitialize on window resize
    };

    // Handles quick sidebar settings
    var handleQuickSidebarSettings = function () {
        var wrapper = $('.page-quick-sidebar-wrapper');
        var wrapperAlerts = wrapper.find('.page-quick-sidebar-settings');

        var initSettingsSlimScroll = function () {
            var settingsList = wrapper.find('.page-quick-sidebar-settings-list');
            var settingsListHeight;

            settingsListHeight = wrapper.height() - wrapper.find('.nav-justified > .nav-tabs').outerHeight();

            // alerts list 
            Metronic.destroySlimScroll(settingsList);
            settingsList.attr("data-height", settingsListHeight);
            Metronic.initSlimScroll(settingsList);
        };

        initSettingsSlimScroll();
        Metronic.addResizeHandler(initSettingsSlimScroll); // reinitialize on window resize
    };

    var refreshPendingNotifications = function(){
        $('#refreshPendingNotification').click(function () {
            angular.element(document.getElementById('quicksidebarController')).scope().refreshPendingNotification();
        });
    }
    return {

        init: function () {
            //layout handlers
            handleQuickSidebarToggler(); // handles quick sidebar's toggler
            handleQuickSidebarChat(); // handles quick sidebar's chats
            handleQuickSidebarAlerts(); // handles quick sidebar's alerts
            handleQuickSidebarSettings(); // handles quick sidebar's setting
            refreshPendingNotifications(); //
        }
    };

}();