(function (angular) {
    'use strict';

    angular.module('MetronicApp').
    controller('trainingController', trainingController);

    trainingController.$inject = ['$scope', 'userFactory'];
    function trainingController($scope, userFactory) {

        $scope.env = {
            user: null,
            trainig: [],
            weeks: [],
            filters: {
                start_date: null,
                end_date: ''
            }
        };

        userFactory.auth(function (user) {


            $scope.env.user = user;

            if (user.isAthlete()) {
                user.getCurrentTraining(function (rows) {
                    $scope.env.weeks = rows;
                    var training = [];
                    angular.forEach(rows, function (row) {
                        for (var i in [0, 1, 2, 3, 4, 5, 6]) {
                            training.push(
                                {
                                    day: row.getDay(i),
                                    week: row.get(row.dayField(i)),
                                    '1stWeek': row.getWeek(0, i),
                                    '2stWeek': row.getWeek(1, i),
                                    '3rdWeek': row.getWeek(2, i),
                                    '4thWeek': row.getWeek(3, i),
                                }
                            )
                        }
                    });
                    $scope.env.trainig = training;
                    $scope.$apply();
                });
            } else {
                console.log('coach')
            }
        });

        $scope.show = function () {
            var trainig = [];
            var daysLink = {
                1:'dilluns',
                2:'dimarts',
                3:'dimecres',
                4:'dijous',
                5:'divendres',
                6:'dissabte',
                7:'diumenge'
            };
            $scope.env.user.getRangeTraining($scope.env.filters.start_date, $scope.env.filters.end_date, function (rows) {
                var start = moment($scope.env.filters.start_date).hours(0).minutes(0).seconds(0).milliseconds(0);
                var end = moment($scope.env.filters.end_date).hours(0).minutes(0).seconds(0).milliseconds(0);
                //console.log($scope.env.filters.start_date)
                //console.log(start.format('YYYY-MM-DD HH:mm:ss'))
               // console.log(end.format('YYYY-MM-DD HH:mm:ss'))
                //console.log(end.format('YYYY-MM-DD'))
                var days = end.diff(start, 'days')+1;
                var j=0;
                for (var i = 1; i <= days; i++) {
                    var row = rows[j];
                    var numberDay = start.format('E');
                    var stringDay = daysLink[numberDay];

                    if (row!=undefined &&  start.isSameOrAfter( moment(row.get('datini')).hour(0) ) &&  start.isSameOrBefore( moment(row.get('datfi')).hour(0) ) ){

                        start.add(1,'day');
                        if ( numberDay==7 ){
                            j++;
                        }
                        trainig.push(
                            {
                                day: row.getDay(numberDay),
                                week: row.get(stringDay)
                            }
                        )
                        // плюсуем
                        // если доходим до воскресентя, то ставим следующую дату

                    }else{
                        start.add(1,'day');
                    }
                }

                console.log(trainig)
                $scope.env.trainig = trainig;
                $scope.$apply();
            });
        }
    }
})(angular);