(function (angular) {
    'use strict';

    angular.module('MetronicApp').
    controller('requestController', requestListController);

    requestListController.$inject = ['$scope', 'userFactory', 'requestFactory', '$filter','eventFactory'];
    function requestListController($scope, userFactory, requestFactory, $filter, eventFactory) {

        $scope.env = {
            user: null,
            request: [],
            model: {},
            showForm: false,
            saving: false,
            subjects:[]
        };



        userFactory.auth(function (user) {
            $scope.env.user = user;
            user.getRequests(function (requests) {
                $scope.env.requests = requests;
                $scope.$apply();
            })
        });

        $scope.cancelRequest = function () {
            $scope.env.showForm = false;
            $scope.env.saving = false;
        };
        $scope.addRequest = function () {
            $scope.env.showForm = true;
        };

        $scope.send = function (data) {
            $scope.env.saving = true;
            data.user = $scope.env.user.parseObject;
            data.startDate = moment().toDate();
            requestFactory.store(data, function () {
                $scope.env.user.getRequests(function (requests) {
                    $scope.env.requests = requests;
                    alertify.success($filter('translate')('Request created'), "", 10);
                    $scope.cancelRequest();
                    $scope.$apply();
                });
            });
        };
        $scope.deleteRequest = function (request) {
            $scope.env.saving = true;
            request.delete(function () {
                $scope.env.user.getRequests(function (requests) {
                    $scope.env.requests = requests;
                    alertify.success($filter('translate')('Request deleted'), "", 10);
                    $scope.$apply();
                });
            });
        };

        $scope.saveRequest = function (request) {
            $scope.env.saving = true;
            request.update({
                subject: request.subject,
                comments: request.comments,
                coachComment: request.coachComment
            }, function () {
                $scope.env.user.getRequests(function (requests) {
                    $scope.env.requests = requests;
                    alertify.success($filter('translate')('Request changed'), "", 10);
                    $scope.$apply();
                });
                eventFactory.createRequest(request.get('user'), request.parseObject )
            });
        };

        $scope.closeRequest = function (request) {
            request.close(function () {
                $scope.env.user.getRequests(function (requests) {
                    $scope.env.requests = requests;
                    alertify.success($filter('translate')('Request closed'), "", 10);
                    $scope.$apply();
                });
            });
        };


        $scope.openRequest = function (request) {
            request.open(function () {
                $scope.env.user.getRequests(function (requests) {
                    $scope.env.requests = requests;
                    alertify.success($filter('translate')('Request opened'), "", 10);
                    $scope.$apply();
                });
            });
        }

        $scope.$watch(
            function() { return $filter('translate')('Urgent') },
            function(newval) {
                $scope.env.subjects = [
                    $filter('translate')('Urgent'),
                    $filter('translate')('Changes'),
                    $filter('translate')('Nutrition'),
                    $filter('translate')('Races'),
                    $filter('translate')('Comments next month'),
                    $filter('translate')('Performance Test'),
                ];
            }
        );

    }
})(angular);