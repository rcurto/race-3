(function (angular) {
    'use strict';

    function parseSelectDirective() {
        return {
            ngModel:'require',
            replace:false,
            restrict: 'E',
            controller:parseSelectController,
            template: '' +
            '<select  ng-disabled="ngDisabled" class="form-control" ng-model="selected" ng-options="row.id as row.title  group by row.group  for row in data">' +

            '<option value="">{{default}}</option>' +
            '</select>'
            ,
            scope:{
                title:'@',
                grouped:'=',
                ngModel:'=',
                ngDisabled:'=',
                options: '=',
                default: '@'
            }
        };

        parseSelectController.$inject = ['$scope'];

        function parseSelectController($scope){
            $scope.data = [];
            $scope.$watchCollection('ngModel',function(value){
                if ( angular.isObject(value) ){
                    $scope.selected = value.id;
                }
            });

            $scope.$watchCollection('selected',function(value){

                if ( !value ){
                    $scope.ngModel = null
                }

                $scope.options.filter(function(parseObject){
                    if ( parseObject.getId()==value){
                        $scope.ngModel = parseObject.parseObject;
                    }
                })
            });

            $scope.$watchCollection('options',function(value){
                if ( !value ){
                    $scope.data = [] ;
                }

                $scope.title = $scope.title==undefined ? 'name' : $scope.title;
                angular.forEach(value, function(row){
                    var el = {
                        id: row.getId(),
                        title: row.get( $scope.title )
                    };
                    if ( $scope.grouped && row.group ){
                        el.group = row.group
                    }
                    $scope.data.push(el)
                })
            })

        }


    }

    angular.module('parseDirective').directive('parseSelect', parseSelectDirective);


})(window.angular);
